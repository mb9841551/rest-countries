import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "../Styles/Search.css"
import { faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import { useContext } from 'react';
import { ThemeContext } from '../context/themeContext';


function Search({ onSearch }) {

    const {theme} = useContext(ThemeContext)

    const handleSearchChange = (event) => {
        console.log(event.target.value);
        onSearch(event.target.value);
    }


    return (
        <>
            <div className={`search ${theme === "Dark" ? "Dark" : ""}`}>
                <FontAwesomeIcon icon={faMagnifyingGlass}/>
                <input type="search"  onChange={(event) => handleSearchChange(event)} placeholder='Search for a country ...' />
            </div>
        </>
    )
}

export default Search
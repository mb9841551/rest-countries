import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMoon } from '@fortawesome/free-regular-svg-icons';
import "../Styles/Header.css"
import { useContext } from 'react';
import { ThemeContext } from '../context/themeContext';

function Header() {

    const { theme, toggleTheme } = useContext(ThemeContext);

    // console.log(theme);


    return (
        <div className= {`headerComponent ${theme === "Dark" ? "Dark" : ""}`}>
            <h2 className="header">Where in the world?</h2>
            <div className="icon-container">
                <FontAwesomeIcon icon={faMoon}/>
                <div className="dark-mode" onClick={toggleTheme}>
                    Dark Mode
                </div>
            </div>
        </div>
    )

}

export default Header;
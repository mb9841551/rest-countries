import React, { useEffect, useContext, useState } from 'react'
import { useNavigate, useParams, Link } from 'react-router-dom'
import "../Styles/IndividualCountry.css"
import { ThemeContext } from '../context/themeContext';




function IndividualCountry() {

    const [countryInfo, setCountryInfo] = useState(null);
    const [borderCountriesInfo, setBorderCountriesInfo] = useState([]);
    const [nativeName, setNativeName] = useState([]);
    const [currency, setCurrency] = useState([]);

    const navigate = useNavigate();
    const { cca3 } = useParams();
    const { theme } = useContext(ThemeContext)

    useEffect(() => {
        fetch(`https://restcountries.com/v3.1/alpha/${cca3}`)
            .then((response) => {
                if (!response.ok) {
                    throw new Error("Error while fetching individual country data");
                }
                return response.json();
            })
            .then((data) => {
                setCountryInfo(data);

                if (data[0].borders) {
                    setBorderCountriesInfo(data[0].borders);
                }

                setNativeName(Object.values(data[0].name.nativeName));
                setCurrency(Object.keys(data[0].currencies));
            })
            .catch((error) => {
                console.error(error);
            })
    }, [cca3]);

    return (
        <>
            {countryInfo && (
                <div className={`country-info ${theme === "Dark" ? "Dark" : ""}`}>
                    <button className={`back-btn ${theme === "Dark" ? "Dark" : ""}`} onClick={() => navigate("/")}>
                        Go back
                    </button>
                    <div className="country-details">
                        <img src={countryInfo[0].flags.png} alt={countryInfo[0].flags.alt} />
                        <div className="country-detail">
                            <h1 className='country-name'>{countryInfo[0].name.common}</h1>
                            <div className="info">
                                <div className="info-left">
                                    <div>
                                        <strong>Native name: </strong>
                                        {nativeName[0].common}
                                    </div>
                                    <div>
                                        <strong>Population: </strong>
                                        {countryInfo[0].population}
                                    </div>
                                    <div>
                                        <strong>Region: </strong>
                                        {countryInfo[0].region}
                                    </div>
                                    <div>
                                        <strong>SubRegion: </strong>
                                        {countryInfo[0].subregion}
                                    </div>
                                    <div>
                                        <strong>Capital: </strong>
                                        {countryInfo[0].capital}
                                    </div>
                                </div>
                                <div className="info-right">
                                    <div>
                                        <strong>Top Level Domain: </strong>
                                        {countryInfo[0].tld}
                                    </div>
                                    <div>
                                        <strong>Currencies: </strong>
                                        {currency[0]}
                                    </div>
                                </div>
                            </div>
                            {borderCountriesInfo.length > 0 && (
                                <div className="border-countries">
                                    <strong>Border: </strong>
                                    {borderCountriesInfo.map((borderCode) => {
                                        return (
                                            <Link
                                                style={{ color: "inherit", background: "inherit" }}
                                                to={`/country/${borderCode}`}
                                            >
                                                <button className={`borderLinks`}>
                                                    {borderCode}
                                                </button>
                                            </Link>
                                        );
                                    })}
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            )}
        </>
    )

}

export default IndividualCountry
import "../Styles/Population.css"
import React , { useContext } from "react"
import { ThemeContext } from "../context/themeContext"

function Population({ setSortPopulation }) {

  const { theme } = useContext(ThemeContext);

  const handleChange = (event) => {
    setSortPopulation(event.target.value);
  }


  return (
    <>
      <div className={`population ${theme === "Dark" ? "Dark" : ""}`}>
        <select onChange={handleChange}>
          <option value="">Population</option>
          <option value="asc">Ascending</option>
          <option value="desc">Descending</option>
        </select>
      </div>

    </>
  )
}

export default Population

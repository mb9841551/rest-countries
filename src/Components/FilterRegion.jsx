import { React , useContext } from "react"
import "../Styles/FilterRegion.css"
import { ThemeContext } from "../context/themeContext";

function FilterRegion({onSearch , regions}) {

    const { theme } = useContext(ThemeContext);

    // console.log({regions});

    const handleRegionChange = (event) => {
        console.log(event.target.value);
        onSearch(event.target.value);
    }


    return (
        <>
            {
                <div className={`filter ${theme === "Dark" ? "Dark" : ""}`} >
                    <select name="select" onChange={(event) => handleRegionChange(event)}>
                        <option value="">Filter by Region</option>
                        {regions.map((region) => (
                            <option key={region} value={region}>{region}</option>
                        ))}
                    </select>
                </div >
            }
        </>
    )
}

export default FilterRegion

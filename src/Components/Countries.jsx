import React, { useContext } from "react"
import "../Styles/Countries.css"
import { useNavigate } from "react-router-dom";
import { ThemeContext } from "../context/themeContext";


function Countries({ countries, input, regionName, subRegionName, sortPopulation, sortArea, error }) {

    const { theme } = useContext(ThemeContext)

    const navigate = useNavigate();

    const filterAndSortCountries = () => {

        let filterCountries = [];
        countries.forEach((country) => {
            if (
                (!input || country.name.common
                    .toLowerCase()
                    .startsWith(input.toLowerCase())) &&
                (!regionName || regionName === country.region) &&
                (!subRegionName || subRegionName === country.subregion)
            ) {
                filterCountries.push(country);
            }
        });

        if (sortPopulation) {
            if (sortPopulation === "asc") {
                filterCountries.sort((a, b) => a.population - b.population);
            }
            else {
                filterCountries.sort((a, b) => b.population - a.population);
            }
        }
        else if (sortArea) {
            if (sortArea === "asc") {
                filterCountries.sort((a, b) => a.area - b.area);
            }
            else if (sortArea === "desc") {
                filterCountries.sort((a, b) => b.area - a.area);
            }
        }

        return filterCountries;
    }

    let filteredCountries = filterAndSortCountries();



    return (
        <div className="error">
            {error ? ("Error while fetching Data") : (
                <div className={`country-component ${theme === "Dark" ? "Dark" : ""}`}>
                    {filteredCountries.length > 0 ? (
                        filteredCountries.map((country) => {
                            const { flags, name, population, region, capital, cca3 } = country;
                            const { png, alt } = flags;
                            const { common } = name;
                            return (
                                <div className={`country ${theme === "Dark" ? "Dark" : ""}`} key={common} onClick={() => navigate(`country/${cca3}`)} >
                                    <img src={png} alt={alt} />
                                    <div className={`content ${theme === "Dark" ? "Dark" : ""}`}>
                                        <h3 className="name">{common}</h3>
                                        <h4>Population: <span>{population}</span></h4>
                                        <h4>Region: <span>{region}</span></h4>
                                        <h4>Capital: <span>{capital}</span></h4>
                                    </div>
                                </div>
                            );
                        })
                    ) : (
                        <p className="display-text">No countries found</p>
                    )}

                </div>
            )}

        </div>

    )
}

export default Countries
import "../Styles/Area.css"
import React , { useContext } from "react"
import { ThemeContext } from "../context/themeContext"


function Area({ setSortArea }) {

    const { theme } = useContext(ThemeContext);

    const handleChange = (event) => {
        setSortArea(event.target.value);
    }

    return (
        <>
            <div className={`area ${theme === "Dark" ? "Dark" : ""}`}>
                <select onChange={handleChange}>
                    <option value="">Area</option>
                    <option value="asc">Ascending</option>
                    <option value="desc">Descending</option>
                </select>
            </div>
        </>
    )
}

export default Area
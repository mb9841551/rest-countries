import React , { useContext } from "react"
import { ThemeContext } from "../context/themeContext"
import "../Styles/FilterSubRegion.css"

function FilterSubRegion({onSearch , subregions=[]}){

    const { theme } = useContext(ThemeContext);

    // console.log(subregions);


    const handleSubRegionChange = (event) => {
        onSearch(event.target.value);
    }


    return (
        <>
        <div className={`filter ${theme === "Dark" ? "Dark" : ""}`}>
        <select name="select" onChange={(event) => handleSubRegionChange(event)}>
                        <option value="">Filter by SubRegion</option>
                        {subregions.map((subregion) => (
                            <option key={subregion} value={subregion}>{subregion}</option>
                        ))}
                    </select>
        </div>

        </>
    )

}

export default FilterSubRegion

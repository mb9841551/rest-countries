import { useState, useEffect, useContext } from 'react'
import './App.css'
import Search from './Components/Search'
import FilterRegion from './Components/FilterRegion'
import FilterSubRegion from './Components/FilterSubRegion'
import Countries from './Components/Countries'
import Loading from './Components/Loading'
import Population from './Components/Population'
import Area from './Components/Area'
import { ThemeContext } from './context/themeContext'



function App() {

  const { theme } = useContext(ThemeContext);
  const [countries, setCountries] = useState([]);
  const [input, setInput] = useState("");
  const [regionName, setRegionName] = useState("");
  const [subRegionName, setSubRegionName] = useState("");
  const [regionAndSubRegionData, setRegionAndSubRegionData] = useState({});
  const [sortPopulation, setSortPopulation] = useState("");
  const [sortArea , setSortArea] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);


  const fetchCountriesData = async () => {

    try {
      const response = await fetch("https://restcountries.com/v3.1/all");
      if (!response.ok) {
        throw new Error("Error occured");
      }
      const data = await response.json();
      return data;
    }
    catch (error) {
      console.error("Error occured");
    }

  }


  useEffect(() => {

    const fetchData = async () => {
      try {
        const countriesData = await fetchCountriesData();
        setCountries(countriesData);
        setLoading(false);

        const data = countriesData.reduce((accumulator, country) => {
          const { region , subregion } = country

          if (!accumulator[region]) {
            accumulator[region] = [];
          }

          if (!accumulator[region].includes(subregion)) {
            accumulator[region].push(subregion);
          }

          return accumulator;
        }, {});
        
        setRegionAndSubRegionData(data)


      }
      catch (error) {
        setError(true);
        setLoading(false)
        console.error(error);

      }
    }
    fetchData();
  }, [])


  const handleSearch = (value) => {
    setInput(value);
  }

  const handleRegion = (value) => {
    setRegionName(value);
    setSubRegionName("");
  }

  const handleSubRegion = (value) => {
    setSubRegionName(value);
  }





  return (
    loading ? (
      <Loading />
    ) : (
      <>
        <div className={`inputComponent ${theme === "Dark" ? "Dark" : ""}`}>

          <Search onSearch={handleSearch} />

          <FilterRegion onSearch={handleRegion} regions={Object.keys(regionAndSubRegionData)} />

          <FilterSubRegion onSearch={handleSubRegion} subregions={regionAndSubRegionData[regionName]} />

          <Population setSortPopulation={setSortPopulation} />

          <Area setSortArea={setSortArea} />
        </div>

        <Countries countries={countries} input={input} regionName={regionName} subRegionName={subRegionName} sortPopulation={sortPopulation} sortArea={sortArea} error={error}/>

      </>
      

    )
  )
}

export default App


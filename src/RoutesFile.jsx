import React from 'react'
import App from './App'
import Header from './Components/Header'
import IndividualCountry from './Components/IndividualCountry'
import { Routes, Route } from "react-router-dom"

function RoutesFile() {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/country/:cca3" element={<IndividualCountry />} />
      </Routes>
    </>
  )
}

export default RoutesFile

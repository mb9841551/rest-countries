import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import Routes from './RoutesFile.jsx'
import ThemeContextProvider from './context/themeContext'



ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <ThemeContextProvider>
        <Routes />
      </ThemeContextProvider>
    </BrowserRouter>
  </React.StrictMode>,
)
